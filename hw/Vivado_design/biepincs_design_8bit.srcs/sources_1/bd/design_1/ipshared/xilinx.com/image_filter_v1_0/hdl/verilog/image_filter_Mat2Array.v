// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2016.1
// Copyright (C) 1986-2016 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module image_filter_Mat2Array (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        img_data_stream_V_dout,
        img_data_stream_V_empty_n,
        img_data_stream_V_read,
        fb_V_address0,
        fb_V_ce0,
        fb_V_we0,
        fb_V_d0
);

parameter    ap_ST_st1_fsm_0 = 4'b1;
parameter    ap_ST_st2_fsm_1 = 4'b10;
parameter    ap_ST_pp0_stg0_fsm_2 = 4'b100;
parameter    ap_ST_st5_fsm_3 = 4'b1000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv6_0 = 6'b000000;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv12_0 = 12'b000000000000;
parameter    ap_const_lv12_32 = 12'b110010;
parameter    ap_const_lv6_32 = 6'b110010;
parameter    ap_const_lv6_1 = 6'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [7:0] img_data_stream_V_dout;
input   img_data_stream_V_empty_n;
output   img_data_stream_V_read;
output  [11:0] fb_V_address0;
output   fb_V_ce0;
output   fb_V_we0;
output  [7:0] fb_V_d0;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg img_data_stream_V_read;
reg fb_V_ce0;
reg fb_V_we0;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [3:0] ap_CS_fsm;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_22;
reg    img_data_stream_V_blk_n;
reg    ap_sig_cseq_ST_pp0_stg0_fsm_2;
reg    ap_sig_46;
reg    ap_reg_ppiten_pp0_it1;
reg    ap_reg_ppiten_pp0_it0;
reg   [0:0] exitcond_i_reg_165;
reg   [5:0] p_2_i_reg_96;
wire   [11:0] next_mul_fu_107_p2;
reg   [11:0] next_mul_reg_151;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_66;
wire   [0:0] exitcond4_i_fu_113_p2;
wire   [5:0] row_V_fu_119_p2;
reg   [5:0] row_V_reg_160;
wire   [0:0] exitcond_i_fu_129_p2;
reg    ap_sig_75;
wire   [5:0] col_V_fu_135_p2;
wire   [11:0] r_V_fu_141_p2;
reg   [11:0] r_V_reg_174;
reg   [5:0] p_i_reg_73;
reg    ap_sig_98;
reg    ap_sig_cseq_ST_st5_fsm_3;
reg    ap_sig_106;
reg   [11:0] phi_mul_reg_84;
wire   [63:0] tmp_i_63_fu_147_p1;
wire   [11:0] p_2_i_cast1_fu_125_p1;
reg   [3:0] ap_NS_fsm;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 4'b1;
#0 ap_reg_ppiten_pp0_it1 = 1'b0;
#0 ap_reg_ppiten_pp0_it0 = 1'b0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((1'b1 == ap_continue)) begin
            ap_done_reg <= 1'b0;
        end else if (((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & ~(1'b0 == exitcond4_i_fu_113_p2))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it0 <= 1'b0;
    end else begin
        if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & ~(1'b0 == exitcond_i_fu_129_p2))) begin
            ap_reg_ppiten_pp0_it0 <= 1'b0;
        end else if (((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & (1'b0 == exitcond4_i_fu_113_p2))) begin
            ap_reg_ppiten_pp0_it0 <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it1 <= 1'b0;
    end else begin
        if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & (1'b0 == exitcond_i_fu_129_p2))) begin
            ap_reg_ppiten_pp0_it1 <= 1'b1;
        end else if ((((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & (1'b0 == exitcond4_i_fu_113_p2)) | ((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & ~(1'b0 == exitcond_i_fu_129_p2)))) begin
            ap_reg_ppiten_pp0_it1 <= 1'b0;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & (1'b1 == ap_reg_ppiten_pp0_it0) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & (1'b0 == exitcond_i_fu_129_p2))) begin
        p_2_i_reg_96 <= col_V_fu_135_p2;
    end else if (((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & (1'b0 == exitcond4_i_fu_113_p2))) begin
        p_2_i_reg_96 <= ap_const_lv6_0;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_sig_cseq_ST_st5_fsm_3)) begin
        p_i_reg_73 <= row_V_reg_160;
    end else if (((1'b1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_98)) begin
        p_i_reg_73 <= ap_const_lv6_0;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_sig_cseq_ST_st5_fsm_3)) begin
        phi_mul_reg_84 <= next_mul_reg_151;
    end else if (((1'b1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_98)) begin
        phi_mul_reg_84 <= ap_const_lv12_0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75))) begin
        exitcond_i_reg_165 <= exitcond_i_fu_129_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        next_mul_reg_151 <= next_mul_fu_107_p2;
        row_V_reg_160 <= row_V_fu_119_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & (1'b0 == exitcond_i_fu_129_p2))) begin
        r_V_reg_174 <= r_V_fu_141_p2;
    end
end

always @ (*) begin
    if (((1'b1 == ap_done_reg) | ((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & ~(1'b0 == exitcond4_i_fu_113_p2)))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_start) & (1'b1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_sig_cseq_ST_st2_fsm_1) & ~(1'b0 == exitcond4_i_fu_113_p2))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (ap_sig_46) begin
        ap_sig_cseq_ST_pp0_stg0_fsm_2 = 1'b1;
    end else begin
        ap_sig_cseq_ST_pp0_stg0_fsm_2 = 1'b0;
    end
end

always @ (*) begin
    if (ap_sig_22) begin
        ap_sig_cseq_ST_st1_fsm_0 = 1'b1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = 1'b0;
    end
end

always @ (*) begin
    if (ap_sig_66) begin
        ap_sig_cseq_ST_st2_fsm_1 = 1'b1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = 1'b0;
    end
end

always @ (*) begin
    if (ap_sig_106) begin
        ap_sig_cseq_ST_st5_fsm_3 = 1'b1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_3 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & (1'b1 == ap_reg_ppiten_pp0_it1) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75))) begin
        fb_V_ce0 = 1'b1;
    end else begin
        fb_V_ce0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & (1'b1 == ap_reg_ppiten_pp0_it1) & (exitcond_i_reg_165 == 1'b0) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75))) begin
        fb_V_we0 = 1'b1;
    end else begin
        fb_V_we0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & (1'b1 == ap_reg_ppiten_pp0_it1) & (exitcond_i_reg_165 == 1'b0))) begin
        img_data_stream_V_blk_n = img_data_stream_V_empty_n;
    end else begin
        img_data_stream_V_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b1 == ap_sig_cseq_ST_pp0_stg0_fsm_2) & (1'b1 == ap_reg_ppiten_pp0_it1) & (exitcond_i_reg_165 == 1'b0) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75))) begin
        img_data_stream_V_read = 1'b1;
    end else begin
        img_data_stream_V_read = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : begin
            if (~ap_sig_98) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : begin
            if (~(1'b0 == exitcond4_i_fu_113_p2)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_2;
            end
        end
        ap_ST_pp0_stg0_fsm_2 : begin
            if (~((1'b1 == ap_reg_ppiten_pp0_it0) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & ~(1'b0 == exitcond_i_fu_129_p2))) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_2;
            end else if (((1'b1 == ap_reg_ppiten_pp0_it0) & ~((1'b1 == ap_reg_ppiten_pp0_it1) & ap_sig_75) & ~(1'b0 == exitcond_i_fu_129_p2))) begin
                ap_NS_fsm = ap_ST_st5_fsm_3;
            end else begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_2;
            end
        end
        ap_ST_st5_fsm_3 : begin
            ap_NS_fsm = ap_ST_st2_fsm_1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

always @ (*) begin
    ap_sig_106 = (1'b1 == ap_CS_fsm[ap_const_lv32_3]);
end

always @ (*) begin
    ap_sig_22 = (ap_CS_fsm[ap_const_lv32_0] == 1'b1);
end

always @ (*) begin
    ap_sig_46 = (1'b1 == ap_CS_fsm[ap_const_lv32_2]);
end

always @ (*) begin
    ap_sig_66 = (1'b1 == ap_CS_fsm[ap_const_lv32_1]);
end

always @ (*) begin
    ap_sig_75 = ((exitcond_i_reg_165 == 1'b0) & (img_data_stream_V_empty_n == 1'b0));
end

always @ (*) begin
    ap_sig_98 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1));
end

assign col_V_fu_135_p2 = (p_2_i_reg_96 + ap_const_lv6_1);

assign exitcond4_i_fu_113_p2 = ((p_i_reg_73 == ap_const_lv6_32) ? 1'b1 : 1'b0);

assign exitcond_i_fu_129_p2 = ((p_2_i_reg_96 == ap_const_lv6_32) ? 1'b1 : 1'b0);

assign fb_V_address0 = tmp_i_63_fu_147_p1;

assign fb_V_d0 = img_data_stream_V_dout;

assign next_mul_fu_107_p2 = (phi_mul_reg_84 + ap_const_lv12_32);

assign p_2_i_cast1_fu_125_p1 = p_2_i_reg_96;

assign r_V_fu_141_p2 = (phi_mul_reg_84 + p_2_i_cast1_fu_125_p1);

assign row_V_fu_119_p2 = (p_i_reg_73 + ap_const_lv6_1);

assign tmp_i_63_fu_147_p1 = r_V_reg_174;

endmodule //image_filter_Mat2Array
