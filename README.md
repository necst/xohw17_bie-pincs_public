**Team number:** XIL-99002

**Project name:** BIE-PInCS

**Date:** 30/06/2017

**Version of uploaded archive:** 1


**University name:** Politecnico di Milano


**Supervisor name:** Marco D. Santambrogio

**Supervisor e-mail:** marco.santambrogio@polimi.it


**Participants:** Bracco Filippo, Di Vece Chiara, Massari Tommaso

**Email:** filippo.bracco@mail.polimi.it, chiara.divece@mail.polimi.it, tommaso.massari@mail.polimi.it


**Board used:** PYNQ-Z1

**Vivado Version:** 2016.1


## Brief description of project:
Concussion is the leading cause of death among young people under 40 years in industrialized  countries. Due to this high incidence it is fundamental to optimally diagnose this disease. 
Aim of BIE-PInCS project is accelerate on FPGA of an OpenCV application for pupillometry measurement, in order to help the neurological assessment. Thanks to the pupil detection and tracking, the pupillometer allows to estimate pupil diameter and  reactivity to light (photopupillary  reflex) in a much more accurate and faster way than a doctor's human eye can do.This will provide accurate values to evaluate the seriousness of a Traumatic Brain Injury (TBI).
OpenCV application is executed and accelerated on a PYNQ-Z1 board, using Python 3 and a Jupyter notebook.


## Description of archive (explain directory structure, documents and source files):

/doc/ - location of design documentation (pdf)


/ip/ - contains Vivado HLS core sources (including c code)


/src/ - contains sources of design

	-> /src/Vivado_design - contains full Vivado project of design
	
	-> /src/vhdl - Vivado HLS vhdl synthesis files
	
	-> /src/verilog - Vivado HLS verilog synthesis files
	
	
/sw/ - location of software application (Application.ipynb)

	-> /sw/Assets/ - contains assets to be used in application and also two example of stimulation video
	
	-> /sw/wrapper/ - contains the python module which allows to call hardware accelerator
	
	-> /sw/wrapper/test/ - an example of how to use module, with also a image of eye
	
	
/hw/ - contains the final bitstream of design and related tcl file 


## Instructions to build and test project

### Prerequisites:
* PYNQ-Z1 board
* Computer with compatible browser
* Ethernet cable 
* Micro-SD card with preloaded PYNQ-Z1 image (download here: http://www.pynq.io)

1.  Setup the device (detailed guide here: https://pynq.readthedocs.io/en/latest/1_getting_started.html#prerequisites)
2.  Copy into Pynq/Overlay both files in hw folder (biepincs.bit and biepincs.tcl)
3.  Copy Assets folder and Application.ipynb in the same path on Pynq; open application with jupyter and start kernel.

If you want to use hardware accelerator in another application: copy wrapper_imagefilter.py module (it is in /sw/wrapper/) into Pynq folder and import it in your application. Example in /sw/wrapper/test/ shows the use of module.



## Link to YouTube Videos:

**Link to brief presentation video:** https://www.youtube.com/watch?v=Z37EZpHplYw&feature=youtu.be

**BIE-PInCS playlist:** https://www.youtube.com/playlist?list=PLewc2qlpcOudk0TcxfQjREWd_wpiFVJA2

**Project presentation:** https://www.youtube.com/watch?v=_Y6DXl6Wkps

**Rationale behind FPGA:** https://www.youtube.com/watch?v=McwEWrctncM&t=5s

**Work organisation:** https://www.youtube.com/watch?v=g0a0Mu6orOk

**Team presentation:** https://www.youtube.com/watch?v=6z6XzCS58Eg&index=4&t=7s&list=PLewc2qlpcOudk0TcxfQjREWd_wpiFVJA2

**Implementation:** https://www.youtube.com/watch?v=1Vrv63h-xis&list=PLewc2qlpcOudk0TcxfQjREWd_wpiFVJA2&index=5
